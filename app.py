import random
import sys
import os
import datetime
from browser import TermoooBrowser

def obter_palavras(filename = 'palavras.txt'):
    ''' Obter palavras do arquivo base '''

    arquivo = open(filename, 'r')

    conteudo = arquivo.read()

    palavras = conteudo.split("\n")

    arquivo.close()

    return palavras

class TermoooBot():

    def __init__(
        self,
        browser = None,
        headless_browser = True,
        palpite_inicial = None,
        max_tentativas = 6,
        palavra_correta = None,
        log = True
    ):

        if not palpite_inicial:
            palpite_inicial = self.obter_novo_palpite()

        self.palpite_inicial = palpite_inicial

        self.palpites = [palpite_inicial]

        self.tentativa = 1

        self.max_tentativas = max_tentativas

        self.browser = None
        self.palavra_correta = None

        self.letras_ignoradas = []

        self.log = log

        self.representacao_resultado = []

        if palavra_correta:
            self.palavra_correta = palavra_correta

        elif browser:

            browser = TermoooBrowser(headless = headless_browser) 
            
            browser.reiniciar()
            
            self.browser = browser

        else:
            raise Exception('É necessário especificar o browser ou a palavra correta!')

    def __log(self, mensagem):
        ''' Escrever log '''

        if self.log:
            print(str(self.tentativa) + '/' + str(self.max_tentativas) + ' - ' + mensagem)

    def __copiar_apresentacao_resultado(self):
        ''' Copiar apresentação do resultado como é feito no site '''

        today = datetime.date.today()
        someday = datetime.date(2022, 2, 24)

        diff = today - someday
        days_diff = diff.days

        game_index = 53 + days_diff

        text = "joguei term.ooo #" + str(game_index) + " " + str(self.tentativa) + "/6\n"

        for rep in self.representacao_resultado:
            text += ("\n" + rep)

        print("\n" + text)

    def __testar_palavra(self, palavra):
        ''' Obter resultado (cor) de cada letra da palavra testada '''
        
        resultado = []
        resultado_cores = []

        amarelo_count = {}

        if self.browser:
        
            resposta = self.browser.escrever_palpite(palavra)

            cores = {
                'verde': '🟩',
                'amarelo': '🟨',
                'preto': '⬛'
            }

            if resposta['status'] == True:
                
                resultado = resposta['dica']

                for r in resultado:
                    cor = r['cor']
                    resultado_cores.append(cores[cor])

            else:
                raise Exception(resposta['erro'])

        elif self.palavra_correta:

            for index, letra in enumerate(palavra):

                verde_check = letra == self.palavra_correta[index]
                preto_check = not letra in self.palavra_correta
                amarelo_check = letra in self.palavra_correta and self.palavra_correta[index] != letra

                if amarelo_check:
                    
                    if not letra in amarelo_count:
                        amarelo_count[letra] = 0

                    amarelo_count[letra] += 1

                    if amarelo_count[letra] > self.palavra_correta.count(letra):
                        amarelo_check = False
                        preto_check = True

                if verde_check:
                    resultado.append({ 'letra': letra, 'cor': 'verde' })
                    resultado_cores.append('🟩')

                elif preto_check:
                    resultado.append({ 'letra': letra, 'cor': 'preto' })
                    resultado_cores.append('⬛')

                elif amarelo_check:
                    resultado.append({ 'letra': letra, 'cor': 'amarelo' })
                    resultado_cores.append('🟨')

        representacao_resultado = ''.join(resultado_cores)

        self.representacao_resultado.append(representacao_resultado)

        self.__log('Representação do resultado: ' + representacao_resultado)

        return resultado

    def __obter_filtros(self, estrutura):
        ''' Transformar estrutura do resultado obtido no método anterior em uma pesquisa '''

        pesquisa = []
        
        letras_ok = []

        outra_posicao_count = {}

        for index, item in enumerate(estrutura):
            
            letra = item['letra']
            
            tmp = item
            
            tmp['index'] = index

            if item['cor'] == 'preto':

                if letra not in self.letras_ignoradas and letra not in letras_ok:
                    self.letras_ignoradas.append(letra)

            else:

                letras_ok.append(letra)

                if letra in self.letras_ignoradas:
                    self.letras_ignoradas.remove(letra)

                if item['cor'] == 'verde':
                    tmp['condicao'] = 'mesma_posicao'

                elif item['cor'] == 'amarelo':

                    tmp['condicao'] = 'outra_posicao'

                    if not letra in outra_posicao_count:
                        outra_posicao_count[letra] = 0

                    outra_posicao_count[letra] += 1

                del tmp['cor']

                pesquisa.append(tmp)

        for letra in self.letras_ignoradas:
            if letra not in letras_ok:
                pesquisa.append({'condicao': 'nao_tem', 'letra': letra})

        for letra in outra_posicao_count:
            if outra_posicao_count[letra] > 1:
                pesquisa.append({'condicao': 'qtde', 'letra': letra, 'valor': outra_posicao_count[letra]})

        return pesquisa

    def __encontrar_alternativas(self, filtros):
        ''' Obter alternativas possíveis de acordo com o filtro obtido '''

        if len(self.letras_ignoradas) > 0:
            self.__log('Letras ignoradas: ' + ', '.join(self.letras_ignoradas))

        todas_as_palavras = obter_palavras()

        palavras_banidas = obter_palavras('palavras-banidas.txt')

        palavras_possiveis = []

        for palavra in todas_as_palavras:

            # Bloquear palavra vazia ou que já foi testada
            if palavra in self.palpites or palavra in palavras_banidas:
                continue

            possivel = []

            for filtro in filtros:

                letra = filtro['letra']
                
                if filtro['condicao'] == 'nao_tem':

                    tmp = False

                    for filtro_tmp in filtros:
                        if filtro_tmp['condicao'] != 'nao_tem' and letra == filtro_tmp['letra']:
                            tmp = True
                            break
                    
                    possivel.append(True if tmp else (not letra in palavra))

                else:
                    
                    if filtro['condicao'] == 'qtde':
                        possivel.append(palavra.count(letra) >= filtro['valor'])

                    else:

                        i = filtro['index']

                        if filtro['condicao'] == 'mesma_posicao':
                            possivel.append(letra in palavra and palavra[i] == letra)

                        if filtro['condicao'] == 'outra_posicao':
                            possivel.append(letra in palavra and palavra[i] != letra)

            resultado_possivel = all(possivel)

            for letra in self.letras_ignoradas:
                if letra in palavra:
                    resultado_possivel = False

            if resultado_possivel:
                palavras_possiveis.append(palavra)

        log_text = 'Palavras possíveis: ' + str(len(palavras_possiveis))

        if len(palavras_possiveis) < 5:
            log_text += (' - ' + ', '.join(palavras_possiveis))

        self.__log(log_text)

        return palavras_possiveis

    def __verificar_palpite_correto(self, resultado):
        ''' Verificar se o palpite foi o correto! '''

        tmp = []
        palavra = ''

        for r in resultado:
            palavra += r['letra']
            tmp.append(r['cor'] == 'verde')

        if all(tmp):
            return palavra

        return False

    def __tentar_novo_palpite(self, alternativas = [], incrementar = True):
        ''' Tentar a solução com um novo palpite '''

        if len(alternativas) == 0:
            alternativas = obter_palavras()

        novo_palpite = self.obter_novo_palpite(alternativas)

        self.palpites.append(novo_palpite)
        
        if incrementar:
            self.tentativa += 1

        return self.solucionar(novo_palpite, alternativas)

    def __banir_palavra(self, palavra):
        ''' Banir palavra '''

        self.__log('Banindo a palavra: "' + palavra + '"')

        arquivo = open('palavras-banidas.txt', 'a')

        arquivo.write(palavra + "\n")

        arquivo.close()

    def obter_novo_palpite(self, alternativas = None):
        ''' Encontrar melhor palpite dentre as alternativas '''

        if alternativas == None:
            alternativas = obter_palavras()

        novo_palpite = random.choice(alternativas)

        palavras_banidas = obter_palavras('palavras-banidas.txt')

        if novo_palpite in palavras_banidas:
            return self.obter_novo_palpite(alternativas)
        
        return novo_palpite

    def iniciar():
        ''' Iniciar busca '''

        self.tentativa = 1

        self.representacao_resultado = []

        if self.browser:
            self.browser.reiniciar()

    def solucionar(self, palpite = None, ultimas_alternativas = []):
        ''' Solucionar desafio '''

        if not palpite:
            palpite = self.palpite_inicial

        self.__log('Testando a palavra: "' + palpite + '"')

        try:

            # Testar palpite no navegador
            resultado_teste = self.__testar_palavra(palpite)

        except Exception as e:

            # Tentar novamente com outro palpite
            return self.__tentar_novo_palpite(ultimas_alternativas, False)

        palavra_correta = self.__verificar_palpite_correto(resultado_teste)

        if palavra_correta:

            self.__log('Você descobriu a palavra correta: "' + palavra_correta + '"!')

            self.__copiar_apresentacao_resultado()

            return palavra_correta

        filtros = self.__obter_filtros(resultado_teste)

        alternativas = self.__encontrar_alternativas(filtros)

        if len(alternativas) > 0:
            
            if self.tentativa < self.max_tentativas or self.palavra_correta:
                return self.__tentar_novo_palpite(alternativas)

            else:
                self.__log('Você atingiu o número máximo de tentativas!')

        else:
            self.__log('Não foi possível encontrar alternativas!')

        return None

palpite_inicial = None
palavra_correta = None

if len(sys.argv) > 1 and sys.argv[1] != '/':
    palpite_inicial = sys.argv[1]

if len(sys.argv) > 2:
    palavra_correta = sys.argv[2]

bot = TermoooBot(
    palavra_correta = palavra_correta,
    palpite_inicial = palpite_inicial,
    browser = True,
    headless_browser = True
)

result = bot.solucionar()