# Native imports
import traceback
from time import sleep
from unidecode import unidecode

# Vendor imports
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from pyshadow.main import Shadow

class TermoooBrowser():

	def __init__(self, headless = False):
		
		options = webdriver.FirefoxOptions()

		if headless:
			options.headless = True
			
		driver = webdriver.Firefox(
			options = options,
			log_path = '/tmp/geckodriver.log'
		)

		self.driver = driver

		self.esperar_pos_click = 1

		self.shadow = Shadow(driver)

	def __find_element(self, selector, parent=None, many=False, manyIndex=None):
		''' Encontrar elemento dentro da tela '''

		try:

			if many:

				if parent:				
					result = self.shadow.find_elements(parent, selector)

				else:
					result = self.shadow.find_elements(selector)

				if manyIndex != None:

					if len(result) > manyIndex:
						result = result[manyIndex]

					else:
						result = None

			else:

				if parent:
					result = self.shadow.find_element(parent, selector)

				else:
					result = self.shadow.find_element(selector)

			return result

		except Exception as e:
			print('Não foi possível encontrar o elemento: ' + str(e))
			return None

	def __enviar_comando(self, comando, nr = 1, esperar_pos_click = None):
		''' Enviar comando para a página '''

		btn = self.__find_element('button#kbd_' + comando)

		if btn:
			
			for i in range(nr):
				btn.click()

			if esperar_pos_click == None:
				esperar_pos_click = self.esperar_pos_click

			sleep(esperar_pos_click)

		else:
			raise Exception('Não foi possível encontrar o seguinte comando: "' + comando + '"')

	def __limpar_palpite(self):
		''' Limpar palpite '''

		self.__enviar_comando('backspace', 5)

	def __checar_mensagem(self):
		''' Verificar se há alguma mensagem de erro '''

		msg_element = self.__find_element('#msg')

		if msg_element and msg_element.text:
			
			if msg_element.text == 'essa palavra não é válida':
				return 'INVALID_WORD'

			elif msg_element.text == 'só palavras com 5 letras':
				return 'INVALID_WORD'
		
		return None

	def __obter_dicas(self):
		''' Obter dicas dos palpites anteriores '''

		dicas = []

		rows_elm = self.__find_element('wc-row', many = True)

		for row_elm in rows_elm:

			letters_elm = self.__find_element('div.letter', parent = row_elm, many = True)
			
			dica = []

			for letter_elm in letters_elm:

				letter_elm_class_text = letter_elm.get_attribute('class')
				letter_elm_class_list = letter_elm_class_text.split(' ')

				letra = letter_elm.text.lower()
				letra = unidecode(letra)
	
				dica_letra = {
					"letra": letra,
				}

				if 'right' in letter_elm_class_list:
					dica_letra['cor'] = 'verde'

				elif 'wrong' in letter_elm_class_list:
					dica_letra['cor'] = 'preto'

				elif 'place' in letter_elm_class_list:
					dica_letra['cor'] = 'amarelo'

				if 'cor' in dica_letra:
					dica.append(dica_letra)

			if len(dica) == 5:
				dicas.append(dica)

		return dicas

	def reiniciar(self, palpite = None):
		''' Iniciar navegador '''

		self.driver.get('https://term.ooo/')

		help_elm = self.__find_element('button#help')

		if help_elm:
			help_elm.click()

		if palpite:
			return self.escrever_palpite(palpite)

		return None

	def escrever_palpite(self, palavra):
		''' Escrever palpite '''

		try:

			# Limpar palpite anterior
			self.__limpar_palpite()

			for letra in palavra:
				self.__enviar_comando(letra, esperar_pos_click = 0)

			# Enviar palpite
			self.__enviar_comando('enter', esperar_pos_click = 3)

			''' # Checar mensagem de erro
			mensagem_erro = self.__checar_mensagem()

			if not mensagem_erro:
				pass
			else:
				raise Exception(mensagem_erro) '''

			dicas = self.__obter_dicas()

			if len(dicas) > 0:
	
				ultima_dica = dicas.pop()

				acertou = True

				for dica in ultima_dica:
					if dica['cor'] != 'verde':
						acertou = False
						break

				return {
					'status': True,
					'acertou': acertou,
					'dica': ultima_dica
				}

			else:
				raise Exception('Não foi possível obter as dicas')

		except Exception as e:

			resposta = {
				'status': False,
				'erro': str(e)
			}

			return resposta